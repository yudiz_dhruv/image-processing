function enter() {
    var nInput = document.getElementById("input").value;
    const images = "img"

    for (i = 1; i <= nInput; i++) {
        if (nInput <= 5) {
            //const number = "number";
            const button = "btn";

            //script for numbers of seconds input
            const nSecInput = document.createElement("input");
            
            nSecInput.placeholder = `Enter the second ${i}`;
            //nSecInput.id = `${number}_${i}`;
            nSecInput.id = `number_${i}`;
            nSecInput.className = "form-control";
            nSecInput.style.width = '50%';
            nSecInput.style.marginBottom = '10px';
            //document.body.appendChild(nSecInput);

            //srcipt for numbers of button
            // const nSecBtn = document.createElement('button');

            // nSecBtn.id = `${button}_${i}`;
            // nSecBtn.className = "btn btn-sm btn-primary";
            // nSecBtn.innerText = "Done";
            // nSecBtn.style.width = '5%';
            // nSecBtn.style.marginBottom = '10px';
            //document.body.appendChild(nSecBtn);
            
            //script to make button click dynamic
            const span = document.createElement("span");
            span.innerHTML = `<button id="${button}_${i}" type="submit" onclick="getImage(${i})" class="btn btn-sm btn-dark" style="margin-bottom: 10px;" value="submit">Done</button>`;
            
            //append data inside div
            document.getElementById("display").appendChild(nSecInput);
            //document.getElementById("display").appendChild(nSecBtn);
            document.getElementById("display").appendChild(span);
            
            //script for image tag
            const image = document.createElement("img");
            image.id = `${images}_${i}`;
            image.style.width = "30%";
            image.style.marginBottom = "10px";
            document.getElementById("output").appendChild(image);
            
        }
        else {
            document.write("Please Number must be a less or equal than 5");
            break;
        }
    }
}

var interval;  //create the object
function getImage(number) {
    
    const inputId = 'number_' + number;
    const imgId = 'img_' + number;
    const url = "https://jsonplaceholder.typicode.com/photos";

    let nSecond = document.getElementById(inputId).value;

    fetch(url)
    .then((response) => {
        return response.json(); //json will parse the data
    })
    .then((actualData) => {
        var counter = 0;

        //to continue the flow of first image without stoping
        interval[`interval_${number}`] = setInterval(() => {
            document.getElementById(imgId).src = actualData[counter].url;
            // console.log(second * 1000);
            counter++;
        }, nSecond * 1000);
    })
    .catch((error) => {
        console.log(error);
    });
}





